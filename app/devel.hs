{-# LANGUAGE PackageImports #-}
import "concavetriangle" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
